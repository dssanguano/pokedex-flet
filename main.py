import flet as ft
import aiohttp
import asyncio

pokemon_actual = 0


async def main(page: ft.Page):
    page.window_width = 400
    page.window_height = 800
    page.window_resizable = False
    page.padding = 0

    async def peticion(url):
        async with aiohttp.ClientSession() as session:
            async with session.get(url) as response:
                return await response.json()

    async def get_datos_pokemon(e: ft.ContainerTapEvent):
        global pokemon_actual
        if e.control == flecha_superior:
            pokemon_actual += 1
        else:
            pokemon_actual -= 1

        numero = (pokemon_actual % 150) + 1
        resultado = await peticion(f"https://pokeapi.co/api/v2/pokemon/{numero}")
        datos = f"No. {numero}\nName: {resultado['name']}\n\nType: "
        for elemento in resultado["types"]:
            tipo = elemento["type"]["name"]
            datos += f"\n{tipo}"
        
        datos += "\n\nAbilities:"
        for elemento in resultado["abilities"]:
            habilidad = elemento["ability"]["name"]
            datos += f"\n{habilidad}"
        datos += f"\n\nHeight: {resultado['height']}"
        texto.value = datos
        sprite_url = f"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/{numero}.png"
        imagen.src = sprite_url
        await page.update_async()

    async def parpadeo():
        while True:
            await asyncio.sleep(1)
            luz_azul.bgcolor = ft.colors.BLUE_100
            await page.update_async()
            await asyncio.sleep(0.1)
            luz_azul.bgcolor = ft.colors.BLUE
            await page.update_async()

    luz_azul = ft.Container(
        width=60,
        height=60,
        bgcolor=ft.colors.BLUE,
        border=ft.border.all(),
        border_radius=50,
        left=9,
        top=8,
    )

    boton_azul = ft.Stack(
        [
            ft.Container(
                width=70,
                height=70,
                bgcolor=ft.colors.WHITE,
                border=ft.border.all(),
                border_radius=50,
                left=4,
                top=3,
            ),
            luz_azul,
        ]
    )

    items_superior = [
        ft.Container(boton_azul, width=80, height=80),
        ft.Container(
            width=40,
            height=40,
            border=ft.border.all(),
            bgcolor=ft.colors.RED_200,
            border_radius=50,
        ),
        ft.Container(
            width=40,
            height=40,
            border=ft.border.all(),
            bgcolor=ft.colors.YELLOW,
            border_radius=50,
        ),
        ft.Container(
            width=40,
            height=40,
            border=ft.border.all(),
            bgcolor=ft.colors.GREEN,
            border_radius=50,
        ),
    ]
    superior = ft.Container(
        content=ft.Row(items_superior),
        width=400,
        height=80,
        margin=ft.margin.only(top=40),
    )

    sprite_url = f"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/0.png"
    imagen = ft.Image(src=sprite_url, scale=7, width=30, height=30, top=110, right=170)

    tele_pokemon = ft.Stack(
        [
            ft.Container(
                width=310,
                height=250,
                bgcolor=ft.colors.WHITE,
                border=ft.border.all(),
                border_radius=20,
                left=20,
            ),
            ft.Container(
                width=280,
                height=220,
                bgcolor=ft.colors.BLACK,
                border=ft.border.all(),
                left=35,
                top=15,
            ),
            imagen,
        ]
    )
    centro = ft.Container(
        tele_pokemon, width=400, height=250, margin=ft.margin.only(top=40)
    )

    triangulo = ft.canvas.Canvas(
        [
            ft.canvas.Path(
                [
                    ft.canvas.Path.MoveTo(35, 10),
                    ft.canvas.Path.LineTo(10, 50),
                    ft.canvas.Path.LineTo(60, 50),
                ],
                paint=ft.Paint(style=ft.PaintingStyle.FILL, color=ft.colors.WHITE),
            ),
        ],
        width=60,
        height=60,
    )

    flecha_superior = ft.Container(
        triangulo,
        width=80,
        height=70,
        border=ft.border.all(),
        margin=ft.margin.only(top=45),
        border_radius=10,
        bgcolor=ft.colors.BLACK,
        on_click=get_datos_pokemon,
    )

    flechas = ft.Column(
        (
            flecha_superior,
            ft.Container(
                triangulo,
                rotate=ft.Rotate(angle=3.14159),
                width=80,
                height=70,
                border=ft.border.all(),
                border_radius=10,
                bgcolor=ft.colors.BLACK,
                on_click=get_datos_pokemon,
            ),
        )
    )
    texto = ft.Text(value="POKEDEX", color=ft.colors.BLACK, size=12)
    items_inferior = [
        ft.Container(
            texto,
            padding=10,
            width=250,
            height=250,
            border=ft.border.all(),
            bgcolor=ft.colors.GREEN_100,
            border_radius=20,
            margin=ft.margin.only(left=20),
        ),
        ft.Container(flechas, width=70, height=240),
    ]
    inferior = ft.Container(
        content=ft.Row(items_inferior),
        width=400,
        height=250,
        margin=ft.margin.only(top=40),
    )

    col = ft.Column(spacing=0, controls=[superior, centro, inferior])
    contenedor = ft.Container(
        col,
        width=400,
        height=800,
        bgcolor=ft.colors.RED,
        alignment=ft.alignment.top_center,
    )

    await page.add_async(contenedor)
    await parpadeo()


ft.app(target=main)
